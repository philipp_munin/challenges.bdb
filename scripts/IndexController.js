﻿(function () {
    'use strict';
    angular.module('indexController', [])
    .controller('indexController', function IndexController($scope, $serverApi, $location, $q) {
        $scope.issues = [];
        $scope.availableTags = ["bug", "issue", "task", "comment"];
        $scope.availableTagsFilter = [];
        $scope.activeFilter = undefined;
        
        function updateTags() {
            $serverApi.getAllTags().then(function (tags) {
                $scope.availableTagsFilter = tags;
            });
        };
        updateTags();

        $scope.deleteIssue = function (issue)
        {
            $serverApi.delete(issue).then(function () { 
                var i = $scope.issues.indexOf(issue);
                if (i > -1) $scope.issues.splice(i, 1);
                updateTags();
            });
        };
        $scope.addIssue = function (issue) {
//            issue.id = Math.floor(Math.random()*100000);
            $serverApi.update(issue).then(function (updateIssue) {
                $scope.issues.push(issue);
                angular.copy(updateIssue, issue);
                updateTags();
            });
        };
        $scope.saveIssue = function (issue) {
            //$serverApi.saveAll($scope.issues);
            $serverApi.update(issue).then(function (updatedIssue) { 
                angular.copy(updatedIssue, issue);
                updateTags();
            });
        };
        $scope.filterBy = function (tag) {
            $scope.activeFilter = tag;
            $serverApi.get($scope.activeFilter).then(function (issues) {
                $scope.issues = issues;
            });
        };

        $scope.filterBy();
    });
})();