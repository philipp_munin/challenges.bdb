﻿(function () {
    'use strict';
    var issueStates = {
        display: "display",
        edit: "edit",
        insert: "insert"
    };

    angular.module('issue', [])
        .directive('issue', function () {
            return {
                templateUrl: 'issue.html',
                //replace:true,
                transclude: true,
                //controllerAs: 'stringIdentifier',
                controller: function ($scope, $element, $attrs) {
                    $scope.editor = {};
                    $scope.state = $attrs['state'] || issueStates.display;
                    $scope.edit = function () {
                        $scope.editor = angular.copy($scope.issue);
                        $scope.state = issueStates.edit;
                    }
                    $scope.display = function () {
                        $scope.state = issueStates.display;
                        $scope.editor = {};
                    }
                    $scope.cancel = function () {
                        if ($scope.state == issueStates.insert)
                        {
                            $scope.editor = {};
                            return;
                        }
                        $scope.display();
                    };
                    $scope.delete = function () {
                        $scope.$parent.deleteIssue($scope.issue);
                    }
                    $scope.save = function () {
                        if ($scope.state == 'edit') {
                            angular.copy($scope.editor, $scope.issue);
                            $scope.$parent.saveIssue($scope.issue);
                            $scope.display();
                        }
                        else if ($scope.state == 'insert')
                        {
                            $scope.addIssue($scope.editor);
                            $scope.editor = {};
                        }
                    }
                }
            };
        //}
    });
})();