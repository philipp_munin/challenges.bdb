﻿//service issueStorage
(function () {
    'use strict';
    angular.module("serverApi", []).factory('$serverApi', function ($q) {
        var storageId = "serverApiStorage";
        var defaultItems = [
            { id: 1, title: "this is issue 1", description:'Hallo <b>w</b>orld', tags:['bug']},
            { id: 2, title: "this is issue 2", description: 'Hallo <b>world</b> 2', tags:['task','comment'] }
        ];

        function loadAllIssues()
        {
            return JSON.parse(localStorage.getItem(storageId)) || angular.copy(defaultItems);
        }
        function saveAllIssues(issues) {
            localStorage.setItem(storageId, JSON.stringify(issues));
        }
        function findIssueById(issues, id)
        {
            return _.find(issues, function (i) { return i.id == id });
        }


        return {
            get: function (tag) {
                return $q(function (resolve, reject) {
                    setTimeout(function () {
                        var storedIssues = loadAllIssues();
                        
                        if(tag)
                            storedIssues = _.filter(storedIssues, function (i) { 
                                return i.tags && _.contains(i.tags,tag);
                            });
                        resolve(storedIssues);
                    }, 500);
                });
            },
            saveAll: function (issues) {
                return $q(function (resolve, reject) {
                    saveAllIssues(issues);
                    resolve();
                });
            },
            update: function (issue) {
                return $q(function (resolve, reject) {
                    var storedIssues = loadAllIssues();
                    var id = issue.id;
                    if (id) {
                        //find issue by id and update it
                        var storedIssue = findIssueById(storedIssues, id);
                        if (!storedIssue)
                            reject("issue not found by id " + id);
                        else {
                            angular.copy(issue, storedIssue);
                            saveAllIssues(storedIssues);
                        }
                    }
                    else
                    {
                        //new issue
                        issue.id = Math.floor(Math.random() * 100000);
                        storedIssues.push(issue);
                        saveAllIssues(storedIssues);
                    }

                    resolve(angular.copy(issue));
                    console.log("serverApi.update() returned result");
                });
            },

            delete: function (issue)
            {
                return $q(function (accept, reject) {
                    var storedIssues = loadAllIssues();
                    var storedIssue = findIssueById(storedIssues, issue.id);
                    if (storedIssue) {
                        storedIssues = _.without(storedIssues, storedIssue);
                        saveAllIssues(storedIssues);
                    }
                    accept();
                });
            },
            getAllTags: function () {
                return $q(function (accept, reject) {
                    var storedIssues = loadAllIssues();
                    var allTags = _.unique(_.flatten(_.pluck(storedIssues, 'tags')));
                    allTags = _.without(allTags, undefined);
                    accept(allTags);
                });
            }
        };
    });
})();