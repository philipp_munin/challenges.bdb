﻿var connect = require('connect');
var serveStatic = require('serve-static');
connect()
    .use(serveStatic(__dirname))
    .listen(process.env.port || 1337, function () {
    console.log('Server running...');
});

//var http = require('http');
//var port = process.env.port || 1337;
//http.createServer(function (req, res) {
//    res.writeHead(200, { 'Content-Type': 'text/plain' });
//    res.end('Hello World\n');
//}).listen(port);